TYPE = type
type = function(v)
    if TYPE(v) == "table" then
        local meta = getmetatable(v)
        if TYPE(meta) == "table" then
            if TYPE(meta.__name) == "string" then
                return meta.__name
            end
        end
    end
    return TYPE(v)
end

local ccui = {}
ccui.Style = require "ccui.lib.style"
ccui.Element = require "ccui.lib.element"
ccui.Box = require "ccui.lib.box"
ccui.Text = require "ccui.lib.text"
ccui.Button = require "ccui.lib.button"
ccui.Checkbox = require "ccui.lib.checkbox"
ccui.Input = require "ccui.lib.input"
ccui.prompt = require "ccui.lib.prompt"
return ccui