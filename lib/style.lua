local Style = {
    mt = {
        __name = "ccui.style",
    }
}
---@param configs table<string, any>
---@return ccui.style
function Style.new(configs)
    ---@class ccui.style
    return setmetatable({
        configs = configs,
    }, Style.mt)
end

return Style