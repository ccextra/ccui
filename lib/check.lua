local check = {}

---@alias colors.names "white"|"black"|"gray"|"lightGray"|"red"|"blue"|"green"|"pink"|"cyan"|"magenta"|"purple"|"yellow"|"orange"|"lime"|"lightBlue"|"brown"
check.colors = {}
for name, color in pairs(colors) do
    table.insert(check.colors, color)
    table.insert(check.colors, name)
end

---@param t table<string, any>
---@param std table<string, any>
---@param types table<string, string|table<integer, string>>
---@param typeValues table<string, any|table<integer, any>>
function check.fields_std(t, std, types, typeValues)
    for k, v in pairs(t) do
        if std[k] then
            t[k] = v or std[k]
        end
        if types[k] then
            if type(types[k]) == "table" then
                local found = false
                ---@diagnostic disable-next-line: param-type-mismatch
                for _, ty in ipairs(types[k]) do
                    if type(v) == ty then
                        found = true break
                    end
                end
                if not found then
                    ---@diagnostic disable-next-line: param-type-mismatch
                    error("expected "..k.." to be of type "..table.concat(types[k], "|").." (got "..type(v)..")", 3)
                end
            else
                if type(v) ~= types[k] then
                    error("expected "..k.." to be of type "..types[k].." (got "..type(v)..")", 3)
                end
            end
        end
        if typeValues[k] then
            if type(typeValues[k]) == "table" then
                local found = false
                ---@diagnostic disable-next-line: param-type-mismatch
                for _, value in ipairs(typeValues[k]) do
                    if v == value then
                        found = true break
                    end
                end
                if not found then
                    ---@diagnostic disable-next-line: param-type-mismatch
                    error(("expected %s to be %s (got %q)"):format(k, table.concat(typeValues, "|"), v), 3)
                end
            else
                if v ~= typeValues[k] then
                    error(("expected %s to be %s (got %q)"):format(k, typeValues, v), 3)
                end
            end
        end
    end
end
---@param value any
---@param name string
---@param ... string
function check.param_type(value, name, ...)
    local types = {...}
    for _, ty in ipairs(types) do
        if type(value) == ty then
            return
        end
    end
    error("expected "..name.." to be of type "..table.concat(types, "|").." (got "..type(value)..")", 3)
end
---@param value any
---@param name string
---@param ... string
function check.param_value(value, name, ...)
    local values = {...}
    for _, v in ipairs(values) do
        if value == v then
            return
        end
    end
    error(("expected %s to be %s (got %q)"):format(name, table.concat(values, "|"), value), 3)
end

return check