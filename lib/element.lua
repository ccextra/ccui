local check = require "ccui.lib.check"

local Element = {
    mt = {
        __name = "ccui.element"
    }
}
---@param content table<integer, string|ccui.element>
---@param options table
---@param style ccui.style
---@return ccui.element
function Element.new(content, options, style)
    check.param_type(content, "argument #1", "table")
    check.param_type(options, "argument #2", "table")
    check.param_type(style, "argument #3", "ccui.style")
    ---@class ccui.element
    return setmetatable({
        content = content,
        options = options,
        style = style
    }, Element.mt)
end

return Element