local check = require "ccui.lib.check"
local Element = require "ccui.lib.element"

local Button = {
    ---@class ccui.button.std
    std = {
        ---@type "aboslute"|"relative"
        position = "relative",
        ---@type string
        text = "button",
        ---@type number|colors.names
        background_color = colors.gray,
        ---@type number|colors.names
        text_color = colors.white
    },
    ---@class ccui.button.types
    types = {
        position = "string",
        text = "string",
        background_color = { "number", "string" },
        text_color = { "number", "string" },
    },
    ---@class ccui.button.types
    typeValues = {
        position = { "absolute", "relative" },
        background_color = check.colors,
        text_color = check.colors,
    }
}
check.fields_std(Button.std, {}, Button.types, Button.typeValues)
---@param options ccui.button.std
function Button.new(content, options, style)
    check.param_type(content, "argument #1", "table")
    check.param_type(options, "argument #2", "table")
    check.param_type(style, "argument #3", "ccui.style")

    check.fields_std(options, Button.std, Button.types, Button.typeValues)

    return Element.new(content, options, style)
end